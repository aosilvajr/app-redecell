import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { MessagesService } from '../services/messages.service';
import { GestaoMembrosService } from '../services/gestao-membros.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage implements OnInit {

  lider: string;
  celulaID: any;
  detalhesMembros: any[] = [];

  celulas: any;

  constructor(
    private navController: NavController,
    private menuCtrl: MenuController,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private messagesServices: MessagesService,
    private gestaoMembrosService: GestaoMembrosService,
    private detalhesCelulaService: DetalhesCelulaService,
    private loginService: LoginService,
  ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);

    this.celulas = this.loginService.celulasCompletas;

    this.activatedRoute.params.subscribe((data) => {
      this.lider = data.lider;
      this.celulaID = data.id;
    });
  }

  ionViewWillEnter() {
    this.messagesServices.presentLoading()
      .then(() => {
        this.detalhesCelulaService.getDetalhesCelula(this.celulaID)
          .subscribe((detalhesCelula: any) => {
            this.detalhesMembros = [];

            for (let i = 0; i < detalhesCelula.membros.length; i++) {
              const element = detalhesCelula.membros[i];

              this.gestaoMembrosService.getArrayDetalhesMembro(element.id)
                .subscribe((detalhesMembro: any) => {
                  const detalhesMembros = detalhesMembro.membro;

                  this.gestaoMembrosService.getPerfis()
                    .subscribe((resPerfis: any) => {
                      const perfis = resPerfis.perfis;

                      const perfil = perfis.find((element2: any) => {
                        return element2.id === detalhesMembros.perfils_id; // Retorna todo o objeto
                      });

                      this.detalhesMembros.push(Object.assign(detalhesMembros,
                        { perfil_nome: perfil.nome },
                      ));
                    });

                  this.messagesServices.dismissLoading();
                });
            }
          });
      });
  }

  async apagarMembro(idMembro: any) {
    const alert = await this.alertController.create({
      header: 'Deletar Membro!',
      message: 'Deseja deletar esse membro?',
      cssClass: 'cssAlert',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.gestaoMembrosService.apagarMembro(idMembro)
              .subscribe((res: any) => {
                this.ionViewWillEnter();

                console.log(res);
              });

            console.log('Confirmado');
          }
        }
      ]
    });

    await alert.present();
  }

  async historicoPage() {
    await this.navController.navigateForward('celulas-historico');
  }

  async relatorioPage() {
    await this.navController.navigateForward('selecionar-relatorio');
  }

  async addMembroPage() {
    await this.navController.navigateForward('adicionar-membro');
  }

  async editarMembroPage(idMembro: any) {
    const alert = await this.alertController.create({
      header: 'Editar Membro!',
      message: 'Deseja editar esse membro?',
      cssClass: 'cssAlert',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.navController.navigateForward(`editar-membro/${idMembro}`);

            console.log('Confirmado');
          }
        }
      ]
    });

    await alert.present();
  }
}
