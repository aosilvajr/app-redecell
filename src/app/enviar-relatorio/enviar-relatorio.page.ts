import { Component, OnInit } from '@angular/core';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login.service';
import { MessagesService } from '../services/messages.service';
import { EnviarRelatorioService } from '../services/enviar-relatorio.service';

@Component({
  selector: 'app-enviar-relatorio',
  templateUrl: './enviar-relatorio.page.html',
  styleUrls: ['./enviar-relatorio.page.scss'],
})
export class EnviarRelatorioPage implements OnInit {

  membros: any[] = [];
  pendente: any;
  idrelatorio: string;

  visitantes = 0;
  oferta = 0;
  encontro = 0;
  fonovisitas = 0;
  frequentadores = 0;

  constructor(
    private navController: NavController,
    private activatedRoute: ActivatedRoute,
    private loginService: LoginService,
    private enviarRelatorioService: EnviarRelatorioService,
    private detalhesCelulaService: DetalhesCelulaService,
    private messagesServices: MessagesService
  ) { }

  ngOnInit() {
    this.messagesServices.presentLoading()
      .then(() => {
        this.activatedRoute.params.subscribe((data) => {
          this.idrelatorio = data.idrelatorio;
        });

        const celulaID = this.detalhesCelulaService.idCelula;

        this.detalhesCelulaService.getDetalhesCelula(celulaID)
          .subscribe((detalhesCelula: any) => {

            this.pendente = detalhesCelula.pendentes.find((element: any) => {
              return element.idrelatorio === this.idrelatorio;
            });

            for (let i = 0; i < detalhesCelula.membros.length; i++) {
              const element = detalhesCelula.membros[i];

              this.membros.push(Object.assign(element, { valor: false }));
            }

            this.messagesServices.dismissLoading();
          });
      });
  }

  async salvarRelatorio() {
    const objectRelatorio = {
      relatorioid: this.idrelatorio,
      pessoas: this.membros,
      visitantes: this.visitantes,
      oferta: this.oferta,
      encontro: this.encontro,
      fonovisitas: this.fonovisitas,
      frequentadores: this.frequentadores,
      pessoa: this.loginService.id,
      chave: this.loginService.chave,
      celula: this.detalhesCelulaService.idCelula,
      versao: 2,
    };

    this.enviarRelatorioService.enviarRelatorio(objectRelatorio)
      .subscribe((res) => {
        console.log(res);

        this.messagesServices.toastMessage('Seu relatório foi enviado com sucesso :)', 'dark')
          .then(() => {
            this.navController.back();
          });
      });

  }
}
