import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EnviarRelatorioPage } from './enviar-relatorio.page';

const routes: Routes = [
  {
    path: '',
    component: EnviarRelatorioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EnviarRelatorioPage]
})
export class EnviarRelatorioPageModule {}
