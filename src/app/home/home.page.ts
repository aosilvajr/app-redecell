import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { NavController, MenuController } from '@ionic/angular';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  celulasCompletas: any;
  detalhesCelula: any[] = [];
  celulaID: any;

  constructor(
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private loginService: LoginService,
    private detalhesCelulaService: DetalhesCelulaService,
    private messagesService: MessagesService
  ) { }

  ngOnInit() {
    this.celulasCompletas = this.loginService.celulasCompletas;
    this.menuCtrl.enable(true);
  }

  ionViewWillEnter() {
    this.messagesService.presentLoading()
      .then(() => {
        this.detalhesCelula = [];

        for (let i = 0; i < this.celulasCompletas.length; i++) {
          const element = this.celulasCompletas[i];

          this.detalhesCelulaService.getDetalhesCelula(element.id)
            .subscribe((detalhesCelula: any) => {

              this.detalhesCelula.push(Object.assign(
                { lider: element.nome, id: element.id },
                { membros: detalhesCelula.membros },
                { pendentes: detalhesCelula.pendentes }
              ));

              this.messagesService.dismissLoading();
            });
        }
      });
  }

  async getDetalhesCelula(celula: any) {
    await this.navCtrl.navigateForward(`detalhes/${celula.id}/${celula.lider}`);
  }
}
