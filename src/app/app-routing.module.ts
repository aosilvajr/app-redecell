import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'detalhes/:id/:lider', loadChildren: './detalhes/detalhes.module#DetalhesPageModule' },
  { path: 'celulas-historico', loadChildren: './celulas-historico/celulas-historico.module#CelulasHistoricoPageModule' },
  { path: 'selecionar-relatorio', loadChildren: './selecionar-relatorio/selecionar-relatorio.module#SelecionarRelatorioPageModule' },
  { path: 'enviar-relatorio/:idrelatorio', loadChildren: './enviar-relatorio/enviar-relatorio.module#EnviarRelatorioPageModule' },
  { path: 'selecionar-relatorio', loadChildren: './selecionar-relatorio/selecionar-relatorio.module#SelecionarRelatorioPageModule' },
  { path: 'adicionar-membro', loadChildren: './adicionar-membro/adicionar-membro.module#AdicionarMembroPageModule' },
  { path: 'editar-membro/:idmembro', loadChildren: './editar-membro/editar-membro.module#EditarMembroPageModule' },
  { path: 'editar-relatorio', loadChildren: './editar-relatorio/editar-relatorio.module#EditarRelatorioPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
