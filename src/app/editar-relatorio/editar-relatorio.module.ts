import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditarRelatorioPage } from './editar-relatorio.page';

const routes: Routes = [
  {
    path: '',
    component: EditarRelatorioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditarRelatorioPage]
})
export class EditarRelatorioPageModule {}
