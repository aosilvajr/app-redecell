import { Component, OnInit } from '@angular/core';
import { EnviarRelatorioService } from '../services/enviar-relatorio.service';
import { NavController } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import { MessagesService } from '../services/messages.service';
import { DetalhesCelulaService } from './../services/detalhes-celula.service';

@Component({
  selector: 'app-editar-relatorio',
  templateUrl: './editar-relatorio.page.html',
  styleUrls: ['./editar-relatorio.page.scss'],
})
export class EditarRelatorioPage implements OnInit {

  relatorio: any;
  membros: any[] = [];
  idrelatorio: string;

  visitantes = 0;
  oferta = 0;
  encontro = 0;
  fonovisitas = 0;
  frequentadores = 0;

  constructor(
    private navController: NavController,
    private enviarRelatorioService: EnviarRelatorioService,
    private loginService: LoginService,
    private messagesService: MessagesService,
    private detalhesCelulaService: DetalhesCelulaService
  ) { }

  ngOnInit() {
    this.relatorio = this.enviarRelatorioService.relatorioEnviado;

    for (let i = 0; i < this.relatorio.pessoas.length; i++) {
      const element = this.relatorio.pessoas[i];

      if (element.presente === '1') {
        this.membros.push(Object.assign({ id: element.pessoas_id }, { nome: element.nome }, { valor: true }));
      } else {
        this.membros.push(Object.assign({ id: element.pessoas_id }, { nome: element.nome }, { valor: false }));
      }
    }

    const valores = this.relatorio.pessoas.find((element: any) => {
      return element.nome === null;
    });

    if (valores.quantidadevisitantes) {
      this.visitantes = valores.quantidadevisitantes;
    } else {
      this.visitantes = 0;
    }

    if (valores.valoroferta) {
      this.oferta = valores.valoroferta;
    } else {
      this.oferta = 0;
    }

    if (valores.quantidadepessoasparaencontro) {
      this.encontro = valores.quantidadepessoasparaencontro;
    } else {
      this.encontro = 0;
    }

    if (valores.quantidadefrequentadores) {
      this.frequentadores = valores.quantidadefrequentadores;
    } else {
      this.frequentadores = 0;
    }
  }

  async salvarRelatorio() {
    const objectRelatorio = {
      acao: 'editar',
      relatorioid: this.relatorio.id,
      pessoas: this.membros,
      visitantes: this.visitantes,
      oferta: 0,
      encontro: 0,
      fonovisitas: this.fonovisitas,
      frequentadores: this.frequentadores,
      pessoa: this.loginService.id,
      chave: this.loginService.chave,
      celula: this.detalhesCelulaService.idCelula,
      versao: 2,
    };

    console.log(objectRelatorio);

    this.enviarRelatorioService.enviarRelatorio(objectRelatorio)
      .subscribe((res) => {
        console.log(res);

        this.messagesService.toastMessage('Seu evento foi editado com sucesso :)', 'dark')
          .then(() => {
            this.navController.back();
          });
      });
  }
}
