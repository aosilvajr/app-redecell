import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnviarRelatorioService {

  private readonly API = environment.API;

  relatorioEnviado: any;

  constructor(
    private httpClient: HttpClient
  ) { }

  enviarRelatorio(relatorio: any) {
    return this.httpClient.post(`${this.API}/salvaRelatorio.php`, relatorio)
      .pipe(
        take(1)
      );
  }

  editarRelatorio(eventoID: any) {
    return this.httpClient.post(`${this.API}/salvaRelatorio.php`, eventoID)
      .pipe(
        take(1)
      );
  }

  getRelatorioEnviado(relatorioEnviado: any) {
    this.relatorioEnviado = relatorioEnviado;
  }
}
