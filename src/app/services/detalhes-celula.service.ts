import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DetalhesCelulaService {

  private readonly API = environment.API;

  idCelula: any;
  celulasDetalhes: any;

  constructor(
    private httpClient: HttpClient,
    private loginService: LoginService
  ) { }

  getDetalhesCelula(celulaID: string) {
    this.idCelula = celulaID;

    return this.httpClient.post(`${this.API}/selecionaCelula.php`, {
      celula: celulaID,
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }
}
