import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoginService } from './login.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GestaoMembrosService {

  private readonly API = environment.API;

  membro: any;
  perfis: any;
  bairros: any;
  detalhesMembros: any[] = [];

  constructor(
    private httpClient: HttpClient,
    private loginService: LoginService
  ) { }

  apagarMembro(idMembro: string) {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, {
      acao: 'apagar',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      membro: idMembro,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }

  editarMembro(membro: any) {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, membro)
      .pipe(
        take(1)
      );
  }

  adicionarMembro(dadosMembro: string) {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, dadosMembro)
      .pipe(
        take(1)
      );
  }

  getDetalhesMembro(idMembro: any) {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, {
      acao: 'getDetalhesMembro',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      membro: idMembro,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }

  getArrayDetalhesMembro(idMembro: any) {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, {
      acao: 'getDetalhesMembro',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      membro: idMembro,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }

  getPerfis() {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, {
      acao: 'getPerfis',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }

  getBairros() {
    return this.httpClient.post(`${this.API}/gestaoMembros.php`, {
      acao: 'carregaBairros',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      versao: 2
    })
      .pipe(
        take(1)
      );
  }
}
