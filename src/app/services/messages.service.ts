import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  loading: any;

  constructor(
    private loadingController: LoadingController,
    private toastCtrl: ToastController
  ) { }

  async toastMessage(message: string, color: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 4000,
      position: 'top',
      animated: true,
      color: color
    });

    await toast.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Aguarde...',
      spinner: 'bubbles',
      cssClass: 'cssLoading'
    });

    await this.loading.present();
  }

  async dismissLoading() {
    await this.loading.dismiss();
  }
}
