import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NavController, MenuController } from '@ionic/angular';
import { MessagesService } from './messages.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly API = environment.API;

  chave: string;
  id: string;
  celulasCompletas: any;

  constructor(
    private httpClient: HttpClient,
    private navCtrl: NavController,
    private menuController: MenuController,
    private messagesService: MessagesService
  ) { }

  login(credentials: any) {
    this.messagesService.presentLoading()
      .then(() => {
        return this.httpClient.post(`${this.API}/login.php`, credentials)
          .pipe(
            take(1)
          )
          .subscribe((res: any) => {
            console.log(res);

            if (res.resultado === 0) {
              this.messagesService.dismissLoading().then(() => {
                this.messagesService.toastMessage(res.mensagem, 'danger');
              });
            } else if (res === 'Erro no Sistema!') {
              this.messagesService.dismissLoading().then(() => {
                this.messagesService.toastMessage(res, 'danger');
              });
            } else if (res.celulas.length > 1) {
              this.id = res.pessoa;
              this.chave = res.chave;
              this.celulasCompletas = res.celulas;

              this.navCtrl.navigateRoot('home')
                .then(() => {
                  this.messagesService.dismissLoading();
                });
            } else if (res.celulas.length === 1) {
              this.id = res.pessoa;
              this.chave = res.chave;
              this.celulasCompletas = res.celulas;

              this.navCtrl.navigateRoot(`detalhes/${res.celulas[0].id}/${res.celulas[0].nome}`)
                .then(() => {
                  this.messagesService.dismissLoading();
                });
            }
          });
      });
  }
}
