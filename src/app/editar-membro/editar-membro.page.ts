import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GestaoMembrosService } from '../services/gestao-membros.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { EnviarRelatorioService } from '../services/enviar-relatorio.service';
import { MessagesService } from '../services/messages.service';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';

@Component({
  selector: 'app-editar-membro',
  templateUrl: './editar-membro.page.html',
  styleUrls: ['./editar-membro.page.scss'],
})
export class EditarMembroPage implements OnInit {

  membro: any;
  idMembro: any;
  perfis: any;
  editForm: FormGroup;

  customAlertOptionsPerfil: any = {
    header: 'Perfil'
  };

  customAlertOptionsSexo: any = {
    header: 'Sexo'
  };

  customAlertOptionsBatizado: any = {
    header: 'Batizado?'
  };

  constructor(
    private navController: NavController,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private gestaoMembrosService: GestaoMembrosService,
    private loginService: LoginService,
    private messagesService: MessagesService,
    private detalhesCelulaService: DetalhesCelulaService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((data) => {
      this.idMembro = data.idmembro;
    });

    this.gestaoMembrosService.getPerfis().subscribe((perfis: any) => {
      this.perfis = perfis.perfis;
    });

    this.gestaoMembrosService.getDetalhesMembro(this.idMembro)
      .subscribe((detalhesMembro: any) => {
        this.membro = detalhesMembro.membro;

        console.log(detalhesMembro.membro);
      });

    this.editForm = this.formBuilder.group({
      acao: 'editar',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      membro: this.idMembro,
      celula: this.detalhesCelulaService.idCelula,
      nome: ['', [Validators.required]],
      email: ['', [Validators.email]],
      data_nascimento: [''],
      sexo: ['', [Validators.required]],
      isbatizado: ['', [Validators.required]],
      data_batismo: [''],
      perfil: ['', [Validators.required]],
      versao: 2
    });
  }

  get nome() { return this.editForm.get('nome'); }
  get email() { return this.editForm.get('email'); }
  get perfil() { return this.editForm.get('perfil'); }
  get sexo() { return this.editForm.get('sexo'); }
  get isbatizado() { return this.editForm.get('isbatizado'); }

  ionViewDidEnter() {
    this.editForm.patchValue({
      nome: this.membro.nome,
      email: this.membro.email,
      data_nascimento: this.membro.data_nascimento,
      sexo: this.membro.sexo,
      isbatizado: this.membro.isbatizado,
      data_batismo: this.membro.data_batismo,
      perfil: this.membro.perfils_id
    });
  }

  salvar(editForm: any) {
    this.gestaoMembrosService.editarMembro(editForm)
      .subscribe((res: any) => {
        console.log(res);

        this.messagesService.toastMessage(`${editForm.nome} foi editado(a) com sucesso ;)`, 'dark')
          .then(() => {
            this.navController.back();
          });
      });
  }

  async navigateBack() {
    await this.navController.back(); // Resolve o problema da tab voltar para o index 0
  }
}
