import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GestaoMembrosService } from '../services/gestao-membros.service';
import { LoginService } from '../services/login.service';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';
import { MessagesService } from '../services/messages.service';
import { NgxViacepService, Endereco, ErroCep } from '@brunoc/ngx-viacep';

@Component({
  selector: 'app-adicionar-membro',
  templateUrl: './adicionar-membro.page.html',
  styleUrls: ['./adicionar-membro.page.scss'],
})
export class AdicionarMembroPage implements OnInit {

  perfis: any;
  bairros: any;
  addForm: FormGroup;

  customAlertOptionsSexo: any = {
    header: 'Sexo',
  };

  customAlertOptionsBatizado: any = {
    header: 'Batizado?'
  };

  customAlertOptionsTelefone: any = {
    header: 'Tipo de Telefone'
  };

  customAlertOptionsPerfil: any = {
    header: 'Perfis'
  };

  customAlertOptionsBairro: any = {
    header: 'Bairros'
  };

  CEPValidator(control: FormControl) {
    const CEP = control.value;

    if (CEP && CEP !== '') {
      const validaCEP = /^[0-9]{8}$/;

      return validaCEP.test(CEP) ? null : { CEPInvalido: true };
    }
    return null;
  }

  constructor(
    private navController: NavController,
    private formBuilder: FormBuilder,
    private gestaoMembrosService: GestaoMembrosService,
    private loginService: LoginService,
    private detalhesCelulaService: DetalhesCelulaService,
    private viacep: NgxViacepService,
    private messagesService: MessagesService
  ) { }

  ngOnInit() {
    this.gestaoMembrosService.getPerfis()
      .subscribe((perfis: any) => {
        this.perfis = perfis.perfis;
      });

    this.gestaoMembrosService.getBairros()
      .subscribe((bairros: any) => {
        this.bairros = bairros.bairros;
      });

    this.addForm = this.formBuilder.group({
      acao: 'inserir',
      chave: this.loginService.chave,
      pessoa: this.loginService.id,
      celula: this.detalhesCelulaService.idCelula,
      nome: ['', [Validators.required]],
      perfil: ['', [Validators.required]],
      email: ['', [Validators.email]],
      tel_numero: ['', [Validators.required]],
      tel_descricao: ['', [Validators.required]],
      data_nascimento: [''],
      data_batismo: [''],
      sexo: ['', [Validators.required]],
      isbatizado: ['', [Validators.required]],
      end_bairro: ['', [Validators.required]],
      end_descricao: ['', [Validators.required]],
      end_numero: ['', [Validators.required]],
      end_complemento: [''],
      end_cep: ['', [Validators.required, this.CEPValidator]],
      senha: ['', [Validators.required]],
      versao: 2
    });

    this.addForm.get('end_cep').statusChanges
      .subscribe((status: any) => {
        if (status === 'VALID') {
          const cepValue = this.addForm.get('end_cep').value;

          this.viacep.buscarPorCep(cepValue)
            .then((endereco: Endereco) => {
              this.addForm.patchValue({
                end_descricao: endereco.logradouro
              });
            }).catch((error: ErroCep) => {
              this.addForm.get('end_descricao').reset();
              this.messagesService.toastMessage(`${error.message}`, 'danger');
            });
        }
      });
  }

  get nome() { return this.addForm.get('nome'); }
  get sexo() { return this.addForm.get('sexo'); }
  get isbatizado() { return this.addForm.get('isbatizado'); }
  get perfil() { return this.addForm.get('perfil'); }
  get tel_descricao() { return this.addForm.get('tel_descricao'); }
  get tel_numero() { return this.addForm.get('tel_numero'); }
  get email() { return this.addForm.get('email'); }
  get senha() { return this.addForm.get('senha'); }

  get end_cep() { return this.addForm.get('end_cep'); }
  get end_bairro() { return this.addForm.get('end_bairro'); }
  get end_descricao() { return this.addForm.get('end_descricao'); }
  get end_numero() { return this.addForm.get('end_numero'); }

  adicionar(addForm: any) {
    this.gestaoMembrosService.adicionarMembro(addForm)
      .subscribe((res: any) => {
        console.log(res);

        this.messagesService.toastMessage(`${addForm.nome} foi adicionado(a) com sucesso ;)`, 'dark')
          .then(() => {
            this.navController.back();
          });
      });
  }
}
