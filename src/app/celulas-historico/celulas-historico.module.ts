import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CelulasHistoricoPage } from './celulas-historico.page';

const routes: Routes = [
  {
    path: '',
    component: CelulasHistoricoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CelulasHistoricoPage]
})
export class CelulasHistoricoPageModule { }
