import { Component } from '@angular/core';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';
import { NavController, AlertController } from '@ionic/angular';
import { EnviarRelatorioService } from '../services/enviar-relatorio.service';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-celulas-historico',
  templateUrl: './celulas-historico.page.html',
  styleUrls: ['./celulas-historico.page.scss'],
})
export class CelulasHistoricoPage {

  historicos: any;

  constructor(
    private navCtrl: NavController,
    private alertController: AlertController,
    private messagesService: MessagesService,
    private detalheCelulasService: DetalhesCelulaService,
    private enviarRelatorioService: EnviarRelatorioService
  ) { }

  ionViewWillEnter() {
    this.messagesService.presentLoading() // Inicia fora do subscribe e finaliza dentro do subscribe
      .then(() => {
        const celulaID = this.detalheCelulasService.idCelula;

        this.detalheCelulasService.getDetalhesCelula(celulaID)
          .subscribe((detalhesCelula: any) => {
            this.historicos = detalhesCelula.passados;

            this.messagesService.dismissLoading();
          });
      });
  }

  async editarRelatorio(relatorioEnviado: any) {
    const alert = await this.alertController.create({
      header: 'Editar Evento!',
      message: 'Deseja editar esse evento?',
      cssClass: 'cssAlert',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.navCtrl.navigateForward('editar-relatorio')
              .then(() => {
                this.enviarRelatorioService.getRelatorioEnviado(relatorioEnviado);
              });

            console.log('Confirmado');
          }
        }
      ]
    });

    await alert.present();
  }
}
