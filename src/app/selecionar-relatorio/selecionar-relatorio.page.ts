import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DetalhesCelulaService } from '../services/detalhes-celula.service';

@Component({
  selector: 'app-selecionar-relatorio',
  templateUrl: './selecionar-relatorio.page.html',
  styleUrls: ['./selecionar-relatorio.page.scss'],
})
export class SelecionarRelatorioPage {

  pendentes: any;

  constructor(
    private navController: NavController,
    private detalhesCelulaService: DetalhesCelulaService
  ) { }

  ionViewWillEnter() {
    const celulaID = this.detalhesCelulaService.idCelula;

    this.detalhesCelulaService.getDetalhesCelula(celulaID)
      .subscribe((detalhesCelula: any) => {
        this.pendentes = detalhesCelula.pendentes;
      });
  }

  async enviarRelatorioPage(id: number) {
    await this.navController.navigateForward(`enviar-relatorio/${id}`);
  }
}
