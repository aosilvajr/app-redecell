import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      // email: ['beenhurg@gmail.com', [Validators.required, Validators.email]],
      // senha: ['020995', [Validators.required]],
      // email: ['dcarlospa@gmail.com', [Validators.required, Validators.email]],
      // senha: ['efraim2017', [Validators.required]],
      // email: ['anderson.ribeiro953@gmail.com', [Validators.required, Validators.email]],
      // senha: ['9898', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required]],
      // email: ['', [Validators.required, Validators.email]],
      // senha: ['', [Validators.required]],
      // versao: 2, 
    });
  }

  get email() { return this.loginForm.get('email'); }
  get senha() { return this.loginForm.get('senha'); }

  login() {
    const credentials = this.loginForm.value;

    this.loginService.login(credentials);
  }
}
